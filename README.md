This demo project is intended to show and display WebAPI functionality supported by Lycia.

You can learn how WebAPI can be created and used from Lycia online documentation:
WebAPI - https://querix.com/go/lycia/#12_lycia_web_api/web_api_overview.htm
WebAPI example - https://querix.com/go/lycia/#12_lycia_web_api/lycia_web_api_practical/practical_implemetation_webapi.htm

To be able to try and use this project, you have to perform the following steps.
1. Register at Querix web-site: https://querix.com/signup/
2. Download the latest Lycia III version: http://querix.com/lycia/downloads/
3. Install Lycia to your computer: https://querix.com/go/lycia/#01_qpm/02_steps.htm
4. Activate the trial license (1 license per 1 developer): https://querix.com/go/lycia/#02_licensing/01_trial.htm (Please contact us if you need more compilation seats. We'd also appreciate it if you let us know the number of developers who will be engaged in this project.)
5. Import this project from the GIT repository via LyciaStudio: https://querix.com/go/lycia/index.htm#05_workbench/01_ls/02_interface/02_views/git/git_repos_view.htm

Use the link below to download the source files. Here is the link to be cloned: https://gitlab.com/QuerixTraining/WebAPI.git

You can read about working with GIT repositories in Lycia online documentation: https://querix.com/go/lycia/index.htm#05_workbench/01_ls/02_interface/02_views/git/git_repos_view.htm

After importing this demo project to your workspace, you have to build and run it from LyciaStudio:
https://querix.com/go/lycia/index.htm#05_workbench/01_ls/04_how_to/03_build/build.htm
https://querix.com/go/lycia/index.htm#05_workbench/01_ls/04_how_to/06_run/run.htm

Please clear your browsing data before launching the application.

######################################

This project contains demo applications for Querix products.
This software is free but can be used and modified only with Lycia.
Feel free to give your suggestions about enriching this site. 
You can send your ideas and 4gl code to support@querix.com