##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

import requests

session = requests.Session()

r = session.post("http://localhost:9090/LyciaWeb/esapi/run/default/01_server_with_menu")

print(r.status_code, r.reason)
print(r.text)

r = session.get("http://localhost:9090/LyciaWeb/esapi/current/actions") 
print(r.status_code, r.reason)
print(r.text)

r = session.post("http://localhost:9090/LyciaWeb/esapi/call/web_calculating_sum", data={"args": "[4,3]"})
print(r.status_code, r.reason)
print(r.text)