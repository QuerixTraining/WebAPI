##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE  rs web.Response
DEFINE  ses web.Session
DEFINE js STRING
DEFINE usd_rec RECORD
			usd_base STRING,
			usd_date STRING,
			rates RECORD
					aud DECIMAL(16,4),
					bgn DECIMAL(16,4),
					brl DECIMAL(16,4),
					cad DECIMAL(16,4),
					chf DECIMAL(16,4),
					cny DECIMAL(16,4),
					czk DECIMAL(16,4),
					dkk DECIMAL(16,4),
					gbp DECIMAL(16,4),
					hkd DECIMAL(16,4),
					hrk DECIMAL(16,4),
					huf DECIMAL(16,4),
					idr DECIMAL(16,4),
					ils DECIMAL(16,4),
					inr DECIMAL(16,4),
					jpy DECIMAL(16,4),
					krw DECIMAL(16,4),
					mxn DECIMAL(16,4),
					myr DECIMAL(16,4),
					nok DECIMAL(16,4),
					nzd DECIMAL(16,4),
					php DECIMAL(16,4),
					pln DECIMAL(16,4),
					ron DECIMAL(16,4),
					rub DECIMAL(16,4),
					sek DECIMAL(16,4),
					sgd DECIMAL(16,4),
					thb DECIMAL(16,4),
					trl DECIMAL(16,4),
					usd DECIMAL(16,4),
					zar DECIMAL(16,4)
				END RECORD	
			END RECORD

    MAIN
            
    CALL web.Request.get("http://api.fixer.io/latest?base=USD").perform() RETURNING rs
	
	DISPLAY "HTTP Response Status Code : "
	DISPLAY rs.GetHTTPCode()
	
    LET js = rs.getBody() 
	DISPLAY "\nResponse JSON string : "
	DISPLAY js

	CALL util.JSON.parse( js, usd_rec )
    DISPLAY "\nCurrency exchange info: "
    DISPLAY "Date : ", usd_rec.usd_date 
    DISPLAY "Rates USD/GBP : ", usd_rec.rates.gbp
	
END MAIN