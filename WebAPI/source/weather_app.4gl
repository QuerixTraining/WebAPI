##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE  rs web.Response
DEFINE  ses web.Session
DEFINE js STRING

MAIN
	
	CALL web.Request.post("http://api.openweathermap.org/data/2.5/forecast/city?APPID=f8e2058ceeea40a24fc0b67f329315da").param("q", "London").param("units", "metric").perform() RETURNING rs
	
	DISPLAY "HTTP Response Status Code : "
	DISPLAY rs.GetHTTPCode()
		
    LET js = rs.getBody() 
	DISPLAY "\nResponse JSON string : "
	DISPLAY js
	DISPLAY "\nFormatted JSON string : " 
    DISPLAY util.JSON.format( js )

END MAIN