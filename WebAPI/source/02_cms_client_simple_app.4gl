##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
   
   DEFINE  rq web.Request
   DEFINE  rs web.Response
   DEFINE  ses web.Session
   DEFINE  url web.URL
   DEFINE  prefix, instance, app, s STRING
   DEFINE  result STRING
   
   CAll url.setHost("online-demos.querix.com")
   --CAll url.setPort(9090)
   LET  prefix = "LyciaWeb/esapi/"
   LET  instance = "run/default"
   LET  app = "cms_2016/cms"
   
   
	MENU
		COMMAND "Get First Contact"
     		CAll url.setPath(prefix || instance || "/" || app)
		   	CALL rq.setMethod("POST")
		   	CAll  rq.setURL(url)
		   	CALL ses.perform(rq) RETURNING rs

			CALL  rq.setMethod("POST")
			CAll url.setPath(prefix||"action/accept")
		    CAll  rq.setURL(url)
		    CALL ses.perform(rq)
			
			IF rs.GetHTTPCode() == "201" OR rs.GetHTTPCode() == "200" THEN 
		   		DISPLAY "Connection - Successful"
				CALL  rq.setMethod("POST")
				CAll url.setPath(prefix||"action/f102")
			    CAll  rq.setURL(url)
			    CALL ses.perform(rq)
	
				CALL  rq.setMethod("GET")
				CAll url.setPath(prefix||"get/contact.cont_lname")
			    CAll  rq.setURL(url)
			    CALL ses.perform(rq) RETURNING rs
				LET result = rs.getBody()
	
				DISPLAY "First name: "||result
	
				CALL  rq.setMethod("GET")
				CAll url.setPath(prefix||"get/contact.cont_fax")
			    CAll  rq.setURL(url)
			    CALL ses.perform(rq) RETURNING rs
				LET result = rs.getBody()
				
				DISPLAY "Fax: "||result
			ELSE 
				DISPLAY "Connection - Failed. \n Try again later."
			END IF
				
			

  		COMMAND "Exit"
  			EXIT MENU
	END MENU 
   
   
END MAIN   