##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

IMPORT SWAGGER "http://localhost:9090/LyciaWeb/esapi/swagger/v2/lycia-web.json" AS swag

MAIN
    DEFINE result INT
    DEFINE code INT
    DEFINE runStatus RECORD arg, sessionid, status STRING END RECORD 
   
    -- START "WEB SERVICE" APPLICATION
    CALL swag.run_instance_command_post("default", "03_web_service") RETURNING code, runStatus.*
    IF code = 201 AND runStatus.status = "Idle" THEN DISPLAY "Start ok. SessionId: " || runStatus.sessionid ELSE DISPLAY "Start fail: " || code END IF

    -- CALL FUNCTION web_sum
    CALL swag.call_functionName_post(runStatus.sessionid, "", "web_sum", "[12,13]") RETURNING code, result
    IF code = 200 THEN DISPLAY "Run function ok. Result is: " || result ELSE DISPLAY "Run function fail" END IF
   
    -- STOP "WEB SERVICE" APPLICATION
    CALL swag.quit_post(runStatus.sessionid) RETURNING code
    IF code = 200 THEN DISPLAY "Stop ok" ELSE DISPLAY "Stop fail" END IF
END MAIN

