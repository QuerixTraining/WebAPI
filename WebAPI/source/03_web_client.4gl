##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN

    DEFINE  ses WEB.SESSION
    DEFINE  rq WEB.Request
    DEFINE  rs WEB.Response
    DEFINE  url web.URL
    DEFINE  prefix, instance, app STRING
    DEFINE  result DYNAMIC ARRAY OF INT
   
    -- PREPARE URL
    CAll url.setHost("localhost")
    CAll url.setPort(9090)
    LET  prefix = "LyciaWeb/esapi/"
    LET  instance = "run/default"
    LET  app = "03_web_service"
    CAll url.setPath(prefix || instance || "/" || app)
   
    -- START "WEB SERVICE" APPLICATION
    CALL rq.setMethod("POST")
    DISPLAY "URL : ", url.getURL()
    CAll rq.setURL(url)
    CALL ses.perform(rq) RETURNING rs
   
    -- CALL FUNCTION web_sum
    CALL rq.setMethod("POST")
    CAll url.setPath(prefix || 'call/web_sum?args=[12,13]')
    CAll rq.setURL(url)
    DISPLAY "URL : ", url.getURL()
    CALL ses.perform(rq) RETURNING rs
    CALL util.JSON.parse( rs.getBody(), result )
    DISPLAY "Result: " || result[1]
 
    -- STOP "WEB SERVICE" APPLICATION
    CALL  rq.setMethod("POST")
    CAll  url.setPath(prefix || "quit")
    DISPLAY "URL : ", url.getURL()
    CAll  rq.setURL(url)
    CALL ses.perform(rq) RETURNING rs

END MAIN

