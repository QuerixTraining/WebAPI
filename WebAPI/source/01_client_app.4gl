##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE  rs web.Response
DEFINE  ses web.Session

MAIN

	CALL web.Request.post("http://localhost:9090/LyciaWeb/esapi/run/default/01_server_with_menu").perform(ses) RETURNING rs
	
	DISPLAY rs.GetHTTPCode()
	DISPLAY rs.getBody()

	CALL web.Request.post("http://localhost:9090/LyciaWeb/esapi/action/show%20error").perform(ses) RETURNING rs
	DISPLAY rs.GetHTTPCode()
	DISPLAY rs.getBody()

	CALL web.Request.get("http://localhost:9090/LyciaWeb/esapi/current/error").perform(ses) RETURNING rs
	DISPLAY rs.GetHTTPCode()
	DISPLAY rs.getBody()
	
	#CALL web.Request.post("http://localhost:9090/LyciaWeb/esapi/call/web_calculating_sum").param("args","[4, 3]").perform(ses) RETURNING rs
    #DISPLAY "Response Status: ", rs.GetHTTPCode()
    #DISPLAY rs.getBody()
END MAIN

