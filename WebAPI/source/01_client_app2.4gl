##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

MAIN
   
   DEFINE  rq web.Request
   DEFINE  rs web.Response
   DEFINE  ses web.Session
   DEFINE  url web.URL
   DEFINE  prefix, instance, app, s STRING
   
   CAll url.setHost("localhost")
   CAll url.setPort(9090)
   LET  prefix = "LyciaWeb/esapi/"
   LET  instance = "run/default"
   LET  app = "01_server_with_menu"
   
   CAll url.setPath(prefix || instance || "/" || app) # 10.38.57.39:9090/LyciaWeb/esapi/run/default/01_server_with_menu
   
   CALL rq.setMethod("POST")
   DISPLAY "URL : \n", url.getURL(), "\n"
   
   CAll  rq.setURL(url)
   CALL ses.perform(rq) RETURNING rs
   
   DISPLAY "STATUS CODE : ", rs.GetHTTPCode(), "\n"
   DISPLAY "BODY : \n", rs.getBody(), "\n"
   
END MAIN  