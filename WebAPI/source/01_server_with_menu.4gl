##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

# Server with MENU and functions


MAIN

	MENU "start_menu"
		COMMAND "Show Error"
			ERROR  "This is error"
		COMMAND "Show Message"
			MESSAGE "This is message"			
		COMMAND "Exit app"
			EXIT MENU
	END MENU			

END MAIN



FUNCTION web_return_string(inputString)
	DEFINE inputString STRING
		RETURN inputString
END FUNCTION

FUNCTION web_calculating_sum(a,b)
	DEFINE a, b INT
		RETURN a + b
END FUNCTION		