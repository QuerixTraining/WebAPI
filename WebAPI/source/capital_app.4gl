##########################################################################
# WebAPI demo                                                            #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE  rs web.Response
DEFINE  ses web.Session

MAIN
	CALL web.Request.get("https://restcountries.eu/rest/v1/capital/dublin").perform(ses) RETURNING rs
	
	DISPLAY "HTTP Response Status Code : "
	DISPLAY rs.GetHTTPCode()
	DISPLAY "\nResponse JSON string : "
    DISPLAY rs.getBody() 
	
END MAIN